/*
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
  @NOTATION@
 */

/*
 * Date editor widget
 *
 * Author: Miguel de Icaza
 * Gtk3-support Boris Rozhkin
 */

#define _XOPEN_SOURCE
#define GETTEXT_PACKAGE "libgnome-dateedit"

#include <time.h>
#include <string.h>
#include <stdlib.h> /* atoi */
#include <stdio.h>

#include <glib/gi18n-lib.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include <gnome-dateedit.h>

#ifdef G_OS_WIN32
/* The use of strtok_r() in this file doesn't require us to use a real
 * strtok_r(). (Which doesn't exist in Microsoft's C library.)
 * Microsoft's strtok() uses a thread-local buffer, not a
 * caller-allocated buffer like strtok_r(). But this is fine for the
 * way it gets used here. To avoid gcc warnings about unused
 * variables, use the third argument to store the return value from
 * strtok().
 */
#define strtok_r(s, delim, ptrptr) (*(ptrptr) = strtok (s, delim))
#endif

GType
gnome_date_edit_flags_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0))
  {
    static const GFlagsValue _gnome_date_edit_flags_values[] = {
      { GNOME_DATE_EDIT_SHOW_TIME, "GNOME_DATE_EDIT_SHOW_TIME", "show-time" },
      { GNOME_DATE_EDIT_24_HR, "GNOME_DATE_EDIT_24_HR", "24-hr" },
      { GNOME_DATE_EDIT_WEEK_STARTS_ON_MONDAY, "GNOME_DATE_EDIT_WEEK_STARTS_ON_MONDAY", "week-starts-on-monday" },
      { GNOME_DATE_EDIT_DISPLAY_SECONDS, "GNOME_DATE_EDIT_DISPLAY_SECONDS", "display-seconds" },
      { 0, NULL, NULL }
    };

    type = g_flags_register_static ("GnomeDateEditFlags", _gnome_date_edit_flags_values);
  }

  return type;
}


struct _GnomeDateEditPrivate {
	GtkWidget *date_entry;
	GtkWidget *date_button;

	GtkWidget *time_entry;
	GtkWidget *time_popup;

	GtkWidget *cal_label;
	GtkWidget *cal_popup;
	GtkWidget *calendar;

	time_t  initial_time;

	int     lower_hour;
	int     upper_hour;

	int     flags;
	
	int	popuped;
};

enum {
	DATE_CHANGED,
	TIME_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_TIME,
	PROP_DATEEDIT_FLAGS,
	PROP_LOWER_HOUR,
	PROP_UPPER_HOUR,
	PROP_INITIAL_TIME
};

static gint date_edit_signals [LAST_SIGNAL] = { 0 };

static void gnome_date_edit_finalize     (GObject            *object);
static void gnome_date_edit_set_property    (GObject            *object,
					  guint               param_id,
					  const GValue       *value,
					  GParamSpec         *pspec);
static void gnome_date_edit_get_property    (GObject            *object,
					  guint               param_id,
					  GValue             *value,
					  GParamSpec         *pspec);

static void create_children (GnomeDateEdit *gde);

/* to get around warnings */
static const char *strftime_date_format = "%x";

/**
 * gnome_date_edit_get_type:
 *
 * Returns the GType for the GnomeDateEdit widget
 */
/* The following macro defines the get_type */
G_DEFINE_TYPE (GnomeDateEdit, gnome_date_edit, GTK_TYPE_BOX)

static void 
gnome_date_edit_init(GnomeDateEdit *gde)
{
	gde->_priv = g_new0(GnomeDateEditPrivate, 1);
	gde->_priv->lower_hour = 0;
	gde->_priv->upper_hour = 23;
	gde->_priv->popuped = 0;
	gde->_priv->flags = GNOME_DATE_EDIT_SHOW_TIME | GNOME_DATE_EDIT_24_HR;
	create_children (gde);
}

static void
hide_popup (GnomeDateEdit *gde)
{
	gtk_widget_hide (gde->_priv->cal_popup);
	gde->_priv->popuped = 0;
}

static void
day_selected (GtkCalendar *calendar, GnomeDateEdit *gde)
{
	char buffer [256];
	guint year, month, day;
	struct tm mtm = {0};
	char *str_utf8;

	gtk_calendar_get_date (calendar, &year, &month, &day);

	mtm.tm_mday = day;
	mtm.tm_mon = month;
	if (year > 1900)
		mtm.tm_year = year - 1900;
	else
		mtm.tm_year = year;

	if (strftime (buffer, sizeof (buffer),
		      strftime_date_format, &mtm) == 0)
		strcpy (buffer, "???");
	buffer[sizeof(buffer)-1] = '\0';

	/* FIXME: what about set time */

	str_utf8 = g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
	gtk_entry_set_text (GTK_ENTRY (gde->_priv->date_entry),
			    str_utf8 ? str_utf8 : "");
	g_free (str_utf8);
	g_signal_emit (gde, date_edit_signals [DATE_CHANGED], 0);
}

static void
day_selected_double_click (GtkCalendar *calendar, GnomeDateEdit *gde)
{
	hide_popup (gde);
}

static gint
delete_popup (GtkWidget *widget, gpointer data)
{
	GnomeDateEdit *gde;

	gde = data;
	hide_popup (gde);

	return TRUE;
}

static gint
key_press_popup (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	GnomeDateEdit *gde;

	if (event->keyval != GDK_KEY_Escape)
		return FALSE;
	
	gde = data;
	g_signal_stop_emission_by_name (widget, "key_press_event");
	hide_popup (gde);

	return TRUE;
}

/* This function is yanked from gtkcombo.c */
static gint
button_press_popup (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	GnomeDateEdit *gde;
	GtkWidget *child;

	gde = data;

	child = gtk_get_event_widget ((GdkEvent *) event);

	/* We don't ask for button press events on the grab widget, so
	 *  if an event is reported directly to the grab widget, it must
	 *  be on a window outside the application (and thus we remove
	 *  the popup window). Otherwise, we check if the widget is a child
	 *  of the grab widget, and only remove the popup window if it
	 *  is not.
	 */
	if (child != widget) {
		while (child) {
			if (child == widget)
				return FALSE;
			child = gtk_widget_get_parent( child );
		}
	}

	hide_popup (gde);

	return TRUE;
}

static void
position_popup (GnomeDateEdit *gde)
{
    GtkAllocation allocation;
    gint x,y;
    gdk_window_get_origin ( gtk_widget_get_window(  gde->_priv->date_button ), &x, &y);
    gtk_widget_get_allocation( gde->_priv->date_button, &allocation );
    x += allocation.x;
    y += allocation.y;
    y += allocation.height;
    gtk_window_move( GTK_WINDOW(gde->_priv->cal_popup), x, y );
}

static void
select_clicked (GtkWidget *widget, GnomeDateEdit *gde)
{
	const char *str;
	GDate *date;
	int month;

	if ( gde->_priv->popuped ) 
	{
	    hide_popup( gde );
	    return;
	}
	
	gde->_priv->popuped = 1;
	
	str = gtk_entry_get_text (GTK_ENTRY (gde->_priv->date_entry));

	date = g_date_new ();
	g_date_set_parse (date, str);
        /* GtkCalendar expects month to be in 0-11 range (inclusive) */
	month = g_date_get_month (date) - 1;

	gtk_calendar_select_month ( GTK_CALENDAR (gde->_priv->calendar), CLAMP (month, 0, 11), g_date_get_year (date) );
        gtk_calendar_select_day ( GTK_CALENDAR (gde->_priv->calendar), g_date_get_day (date) );
	g_date_free (date);

        position_popup (gde);

	gtk_widget_show (gde->_priv->cal_popup);
}

static void
time_hand_changed(GtkWidget *widget, gpointer *data)
{
	GnomeDateEdit *gde = GNOME_DATE_EDIT(data);
	g_signal_emit (gde, date_edit_signals [TIME_CHANGED], 0);
}

static void
date_hand_changed(GtkWidget *widget, gpointer *data)
{
	GnomeDateEdit *gde = GNOME_DATE_EDIT(data);
	g_signal_emit (gde, date_edit_signals [DATE_CHANGED], 0);
}

static void
set_time (GtkWidget *widget, gpointer *date)
{
	GnomeDateEdit *gde = GNOME_DATE_EDIT(date);
	GtkTreeModel *store = gtk_combo_box_get_model( GTK_COMBO_BOX( widget ) );        
	GtkTreeIter iter;
	gtk_combo_box_get_active_iter( GTK_COMBO_BOX( widget ), &iter );
	gchar* time;
	gtk_tree_model_get ( store, &iter, 0, &time, -1);
	gtk_entry_set_text (GTK_ENTRY (gde->_priv->time_entry), time );
	g_free(time);
	g_signal_emit (gde, date_edit_signals [TIME_CHANGED], 0);
}

static void
fill_time_popup (GtkWidget *widget, GnomeDateEdit *gde)
{

    struct tm *mytm;
    time_t t  = time (NULL);
    mytm = localtime ( &t );
    gboolean once = FALSE;

    GtkTreeIter iter1, iter2, *cur_iter;
    cur_iter = NULL;
    GtkTreeStore *store = gtk_tree_store_new ( 1, G_TYPE_STRING );
    gint i, ii;
    gchar str[40];

    store = gtk_tree_store_new (1, G_TYPE_STRING);
    for (i = gde->_priv->lower_hour; i != gde->_priv->upper_hour+1; ++i )
    {
	g_snprintf( str, sizeof(str), "%.2d:00", i );
	gtk_tree_store_append (store, &iter1, NULL);
        gtk_tree_store_set (store, &iter1, 0, str, -1);
	for( ii = 5; ii < 60; ii+=5 )
	{
	    g_snprintf( str, sizeof(str), "%.2d:%.2d", i, ii );
	    gtk_tree_store_append (store, &iter2, &iter1 );
    	    gtk_tree_store_set (store, &iter2, 0, str, -1);
	    if ( mytm->tm_hour == i &&  mytm->tm_min < ii && !once )
	    {
		once = TRUE;
		cur_iter = gtk_tree_iter_copy( &iter2 );	
	    }
	}
    }
    gtk_combo_box_set_model(GTK_COMBO_BOX(gde->_priv->time_popup), GTK_TREE_MODEL (store) );    
    g_object_unref( store );
    gtk_cell_layout_clear( GTK_CELL_LAYOUT (gde->_priv->time_popup) );

    GtkCellRenderer* renderer = gtk_cell_renderer_text_new ();
    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (gde->_priv->time_popup), renderer, TRUE); 
    gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (gde->_priv->time_popup), renderer, "text", 0, NULL);   

    if (cur_iter )
    {
	gtk_combo_box_set_active_iter (GTK_COMBO_BOX (gde->_priv->time_popup), cur_iter );
	gtk_tree_iter_free ( cur_iter );
    }
}

static gboolean
gnome_date_edit_mnemonic_activate (GtkWidget *widget,
				   gboolean   group_cycling)
{
	gboolean handled;
	GnomeDateEdit *gde;
	
	gde = GNOME_DATE_EDIT (widget);

	group_cycling = group_cycling != FALSE;

	if (!gtk_widget_get_sensitive (gde->_priv->date_entry))
		handled = TRUE;
	else
		g_signal_emit_by_name (gde->_priv->date_entry, "mnemonic_activate", group_cycling, &handled);

	return handled;
}

static void
gnome_date_edit_class_init (GnomeDateEditClass *class)
{
	GtkWidgetClass *widget_class = (GtkWidgetClass *) class;
	GObjectClass *gobject_class = (GObjectClass *) class;

	gobject_class->finalize = gnome_date_edit_finalize;
	gobject_class->get_property = gnome_date_edit_get_property;
	gobject_class->set_property = gnome_date_edit_set_property;

	widget_class->mnemonic_activate = gnome_date_edit_mnemonic_activate;
	g_object_class_install_property (gobject_class,
					 PROP_TIME,
					 g_param_spec_ulong ("time",
							     _("Time"),
							     _("The time currently "
							       "selected"),
							     0, G_MAXULONG,
							     0,
							     (G_PARAM_READABLE |
							      G_PARAM_WRITABLE)));

	/* FIXME: Not sure G_TYPE_FLAGS is right here, perhaps we
	 * need a new type, Also think of a better name then "dateedit_flags" */
	g_object_class_install_property (gobject_class,
					 PROP_DATEEDIT_FLAGS,
					 g_param_spec_flags ("dateedit_flags",
							     _("DateEdit Flags"),
							     _("Flags for how "
							       "DateEdit looks"),
							     GNOME_TYPE_DATE_EDIT_FLAGS,
							     GNOME_DATE_EDIT_SHOW_TIME,
							     (G_PARAM_READABLE |
							      G_PARAM_WRITABLE)));
	g_object_class_install_property (gobject_class,
					 PROP_LOWER_HOUR,
					 g_param_spec_int ("lower_hour",
							   _("Lower Hour"),
							   _("Lower hour in "
							     "the time popup "
							     "selector"),
							   0, 24,
							   7,
							   (G_PARAM_READABLE |
							    G_PARAM_WRITABLE)));
	g_object_class_install_property (gobject_class,
					 PROP_UPPER_HOUR,
					 g_param_spec_int ("upper_hour",
							   _("Upper Hour"),
							   _("Upper hour in "
							     "the time popup "
							     "selector"),
							   0, 24,
							   19,
							   (G_PARAM_READABLE |
							    G_PARAM_WRITABLE)));
	g_object_class_install_property (gobject_class,
					 PROP_INITIAL_TIME,
					 g_param_spec_ulong ("initial_time",
							     _("Initial Time"),
							     _("The initial time"),
							     0, G_MAXULONG,
							     0,
							     (G_PARAM_READABLE |
							      G_PARAM_WRITABLE)));

	date_edit_signals [TIME_CHANGED] =
		g_signal_new ("time_changed",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDateEditClass, time_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	date_edit_signals [DATE_CHANGED] =
		g_signal_new ("date_changed",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDateEditClass, date_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	class->date_changed = NULL;
	class->time_changed = NULL;
}

static void
gnome_date_edit_finalize (GObject *object)
{
	GnomeDateEdit *gde;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_DATE_EDIT (object));

	gde = GNOME_DATE_EDIT (object);

	g_free(gde->_priv);
	gde->_priv = NULL;

	//GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
gnome_date_edit_set_property (GObject            *object,
			   guint               param_id,
			   const GValue       *value,
			   GParamSpec         *pspec)
{
	GnomeDateEdit *self;

	self = GNOME_DATE_EDIT (object);

	switch (param_id) {
	case PROP_TIME:
		gnome_date_edit_set_time(self, g_value_get_ulong (value));
		break;
	case PROP_DATEEDIT_FLAGS:
		gnome_date_edit_set_flags(self, g_value_get_flags (value));
		break;
	case PROP_LOWER_HOUR:
		gnome_date_edit_set_popup_range(self, g_value_get_int (value),
						self->_priv->upper_hour);
		break;
	case PROP_UPPER_HOUR:
		gnome_date_edit_set_popup_range(self, self->_priv->lower_hour,
						g_value_get_int (value));
		break;

	default:
		break;
	}
}

static void
gnome_date_edit_get_property (GObject            *object,
			   guint               param_id,
			   GValue             *value,
			   GParamSpec         *pspec)
{
	GnomeDateEdit *self;

	self = GNOME_DATE_EDIT (object);

	switch (param_id) {
	case PROP_TIME:
		g_value_set_ulong (value,
				   gnome_date_edit_get_time(self));
		break;
	case PROP_DATEEDIT_FLAGS:
		g_value_set_flags (value, self->_priv->flags);
		break;
	case PROP_LOWER_HOUR:
		g_value_set_int (value, self->_priv->lower_hour);
		break;
	case PROP_UPPER_HOUR:
		g_value_set_int (value, self->_priv->upper_hour);
		break;
	case PROP_INITIAL_TIME:
		g_value_set_ulong (value, self->_priv->initial_time);
		break;
	default:
		break;
	}
}

/**
 * gnome_date_edit_set_time:
 * @gde: the GnomeDateEdit widget
 * @the_time: The time and date that should be set on the widget
 *
 * Description:  Changes the displayed date and time in the GnomeDateEdit
 * widget to be the one represented by @the_time.  If @the_time is 0
 * then current time is used.
 */
void
gnome_date_edit_set_time (GnomeDateEdit *gde, time_t the_time)
{
	struct tm *mytm;
	char buffer [256];
	char *str_utf8;

	g_return_if_fail(gde != NULL);

	if (the_time == 0)
		the_time = time (NULL);
	gde->_priv->initial_time = the_time;

	mytm = localtime (&the_time);

	/* Set the date */
	if (strftime (buffer, sizeof (buffer), strftime_date_format, mytm) == 0)
		strcpy (buffer, "???");
	buffer[sizeof(buffer)-1] = '\0';

	str_utf8 = g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
	gtk_entry_set_text (GTK_ENTRY (gde->_priv->date_entry), str_utf8 ? str_utf8 : "");
	g_free (str_utf8);

	/* Set the time */
	if (gde->_priv->flags & GNOME_DATE_EDIT_24_HR) {
		if (gde->_priv->flags & GNOME_DATE_EDIT_DISPLAY_SECONDS) 
		{
			
			gtk_entry_set_max_length (GTK_ENTRY (gde->_priv->time_entry), 8);
			if (strftime (buffer, sizeof (buffer), "%H:%M:%S", mytm) == 0)
    				strcpy (buffer, "???");
        	} 
        	else 
        	{
			gtk_entry_set_max_length (GTK_ENTRY (gde->_priv->time_entry), 5);
			if (strftime (buffer, sizeof (buffer), "%H:%M", mytm) == 0)
    				strcpy (buffer, "???");
        	}
	} 
	else 
	{
		if (gde->_priv->flags & GNOME_DATE_EDIT_DISPLAY_SECONDS) 
		{
			gtk_entry_set_max_length (GTK_ENTRY (gde->_priv->time_entry), 5);
			if (strftime (buffer, sizeof (buffer), "%I:%M:%S %p", mytm) == 0)
				strcpy (buffer, "???");
        	} 
        	else 
        	{
			gtk_entry_set_max_length (GTK_ENTRY (gde->_priv->time_entry), 8);
			if (strftime (buffer, sizeof (buffer), "%I:%M %p", mytm) == 0)
				strcpy (buffer, "???");
        	}
	}
	buffer[sizeof(buffer)-1] = '\0';

	str_utf8 = g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
	gtk_entry_set_text (GTK_ENTRY (gde->_priv->time_entry), str_utf8 ? str_utf8 : "");
	g_free (str_utf8);
}
/**
 * gnome_date_edit_set_popup_range:
 * @gde: The GnomeDateEdit widget
 * @low_hour: low boundary for the time-range display popup.
 * @up_hour:  upper boundary for the time-range display popup.
 *
 * Sets the range of times that will be provide by the time popup
 * selectors.
 */
void
gnome_date_edit_set_popup_range (GnomeDateEdit *gde, int low_hour, int up_hour)
{
        g_return_if_fail (gde != NULL);
	g_return_if_fail (low_hour >= 0 && low_hour <= 24);
	g_return_if_fail (up_hour >= 0 && up_hour <= 24);

	gde->_priv->lower_hour = low_hour;
	gde->_priv->upper_hour = up_hour;

        fill_time_popup(NULL, gde);
}

static void
create_children (GnomeDateEdit *gde)
{
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *arrow;

	gde->_priv->date_entry  = gtk_entry_new ();
        g_signal_connect(G_OBJECT(gde->_priv->date_entry), "activate", G_CALLBACK( date_hand_changed ), gde );
        gtk_widget_set_tooltip_text(gde->_priv->date_entry, _("Date. Enter new date and hit 'Enter'.") );

	gtk_widget_set_size_request (gde->_priv->date_entry, 50, -1);
	gtk_entry_set_max_length(GTK_ENTRY(gde->_priv->date_entry), 10);
	gtk_entry_set_width_chars(GTK_ENTRY(gde->_priv->date_entry), 10);
	gtk_box_pack_start (GTK_BOX (gde), gde->_priv->date_entry, FALSE, FALSE, 0);
	gtk_widget_show (gde->_priv->date_entry);


	gde->_priv->date_button = gtk_button_new ();
        gtk_widget_set_tooltip_text(gde->_priv->date_button, _("Open calendar.") );
	g_signal_connect (gde->_priv->date_button, "clicked", G_CALLBACK (select_clicked), gde);
	gtk_box_pack_start (GTK_BOX (gde), gde->_priv->date_button, FALSE, FALSE, 0);


	hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 3);
	gtk_box_set_homogeneous(GTK_BOX(hbox), FALSE ); 
	gtk_container_add (GTK_CONTAINER (gde->_priv->date_button), hbox);
	gtk_widget_show (hbox);

	/* Calendar label, only shown if the date editor has a time field */

	gde->_priv->cal_label = gtk_label_new (_("Calendar"));
	gtk_misc_set_alignment (GTK_MISC (gde->_priv->cal_label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), gde->_priv->cal_label, TRUE, TRUE, 0);
	if (gde->_priv->flags & GNOME_DATE_EDIT_SHOW_TIME)
		gtk_widget_show (gde->_priv->cal_label);

	arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
	gtk_box_pack_start (GTK_BOX (hbox), arrow, TRUE, FALSE, 0);
	gtk_widget_show (arrow);

	gtk_widget_show (gde->_priv->date_button);

	gde->_priv->time_entry = gtk_entry_new ();
        gtk_widget_set_tooltip_text(gde->_priv->time_entry, _("Time. Enter new time and hit 'Enter'.") );
        g_signal_connect(G_OBJECT(gde->_priv->time_entry), "activate", G_CALLBACK( time_hand_changed ), gde );

	gtk_entry_set_max_length(GTK_ENTRY(gde->_priv->time_entry), 8);
	gtk_entry_set_width_chars(GTK_ENTRY(gde->_priv->time_entry), 8);
	gtk_widget_set_size_request (gde->_priv->time_entry, 50, -1);
	gtk_box_pack_start (GTK_BOX (gde), gde->_priv->time_entry, FALSE, FALSE, 0);

	gde->_priv->time_popup = gtk_combo_box_new ();
        gtk_widget_set_tooltip_text(gde->_priv->time_popup, _("Opens time change menu.") );
	g_signal_connect (gde->_priv->time_popup, "changed", G_CALLBACK (set_time), gde);

	gtk_box_pack_start (GTK_BOX (gde), gde->_priv->time_popup, FALSE, FALSE, 0);

	/* We do not create the popup menu with the hour range until we are
	 * realized, so that it uses the values that the user might supply in a
	 * future call to gnome_date_edit_set_time_range
	 */
	g_signal_connect (gde, "realize", G_CALLBACK (fill_time_popup), gde);

	if (gde->_priv->flags & GNOME_DATE_EDIT_SHOW_TIME) {
		gtk_widget_show (gde->_priv->time_entry);
		gtk_widget_show (gde->_priv->time_popup);
	}

	gde->_priv->cal_popup = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_decorated( GTK_WINDOW(gde->_priv->cal_popup), FALSE); 	
	gtk_widget_set_events (gde->_priv->cal_popup, gtk_widget_get_events (gde->_priv->cal_popup) | GDK_KEY_PRESS_MASK );

	g_signal_connect (gde->_priv->cal_popup, "delete_event", G_CALLBACK (delete_popup), gde);
	g_signal_connect (gde->_priv->cal_popup, "key_press_event", G_CALLBACK (key_press_popup), gde);
	g_signal_connect (gde->_priv->cal_popup, "button_press_event", G_CALLBACK (button_press_popup), gde);
	gtk_window_set_resizable (GTK_WINDOW (gde->_priv->cal_popup), FALSE);

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (gde->_priv->cal_popup), frame);
	gtk_widget_show (frame);

	gde->_priv->calendar = gtk_calendar_new ();
	gtk_calendar_set_display_options (GTK_CALENDAR (gde->_priv->calendar), (GTK_CALENDAR_SHOW_DAY_NAMES | GTK_CALENDAR_SHOW_HEADING | GTK_CALENDAR_SHOW_DETAILS | GTK_CALENDAR_SHOW_WEEK_NUMBERS ));
	g_signal_connect (gde->_priv->calendar, "day_selected", G_CALLBACK (day_selected), gde);
	g_signal_connect (gde->_priv->calendar, "day_selected_double_click", G_CALLBACK (day_selected_double_click), gde);
	gtk_container_add (GTK_CONTAINER (frame), gde->_priv->calendar);
        gtk_widget_show (gde->_priv->calendar);

}

/**
 * gnome_date_edit_new:
 * @the_time: date and time to be displayed on the widget
 * @show_time: whether time should be displayed
 * @use_24_format: whether 24-hour format is desired for the time display.
 *
 * Description: Creates a new #GnomeDateEdit widget which can be used
 * to provide an easy to use way for entering dates and times.
 * If @the_time is 0 then current time is used.
 *
 * Returns: a new #GnomeDateEdit widget.
 */
GtkWidget *
gnome_date_edit_new (time_t the_time, gboolean show_time, gboolean use_24_format)
{
	return gnome_date_edit_new_flags (the_time,
					  ((show_time ? GNOME_DATE_EDIT_SHOW_TIME : 0)
					   | (use_24_format ? GNOME_DATE_EDIT_24_HR : 0)));
}

/**
 * gnome_date_edit_new_flags:
 * @the_time: The initial time for the date editor.
 * @flags: A bitmask of GnomeDateEditFlags values.
 *
 * Description:  Creates a new #GnomeDateEdit widget with the
 * specified flags. If @the_time is 0 then current time is used.
 *
 * Returns: the newly-created date editor widget.
 **/
GtkWidget *
gnome_date_edit_new_flags (time_t the_time, GnomeDateEditFlags flags)
{
	GnomeDateEdit *gde;

	gde = g_object_new (GNOME_TYPE_DATE_EDIT, NULL);

	gnome_date_edit_construct(gde, the_time, flags);

	return GTK_WIDGET (gde);
}

/**
 * gnome_date_edit_construct:
 * @gde: The #GnomeDateEdit object to construct
 * @the_time: The initial time for the date editor.
 * @flags: A bitmask of GnomeDateEditFlags values.
 *
 * Description:  For language bindings and subclassing only
 **/
void
gnome_date_edit_construct (GnomeDateEdit *gde, time_t the_time, GnomeDateEditFlags flags)
{
	gnome_date_edit_set_flags (gde, flags);
	gnome_date_edit_set_time (gde, the_time);
}

/**
 * gnome_date_edit_get_time:
 * @gde: The GnomeDateEdit widget
 *
 * Returns the time entered in the GnomeDateEdit widget
 */
time_t
gnome_date_edit_get_time (GnomeDateEdit *gde)
{
	struct tm tm = {0};
	const char *str;
	GDate *date;

	/* Assert, because we're just hosed if it's NULL */
	g_assert(gde != NULL);
	g_assert(GNOME_IS_DATE_EDIT(gde));

	str = gtk_entry_get_text (GTK_ENTRY (gde->_priv->date_entry));

	date = g_date_new ();
	g_date_set_parse (date, str);

	g_date_to_struct_tm (date, &tm);

	g_date_free (date);

	/* FIXME: the preceeding needs further checking to see if it's correct,
	 * the following is so utterly wrong that it doesn't even deserve to be
	 * just commented out, but I didn't want to cut it right now */
#if 0
	sscanf (gtk_entry_get_text (GTK_ENTRY (gde->_priv->date_entry)), "%d/%d/%d",
		&tm.tm_mon, &tm.tm_mday, &tm.tm_year);

	tm.tm_mon = CLAMP (tm.tm_mon, 1, 12);
	tm.tm_mday = CLAMP (tm.tm_mday, 1, 31);

	tm.tm_mon--;

	/* Hope the user does not actually mean years early in the A.D. days...
	 * This date widget will obviously not work for a history program :-)
	 */
	if (tm.tm_year >= 1900)
		tm.tm_year -= 1900;

#endif

	if (gde->_priv->flags & GNOME_DATE_EDIT_SHOW_TIME) {
		char *tokp, *temp;
		char *string;
		char *flags = NULL;

		string = g_strdup (gtk_entry_get_text (GTK_ENTRY (gde->_priv->time_entry)));
		temp = strtok_r (string, ": ", &tokp);
		if (temp) {
			tm.tm_hour = atoi (temp);
			temp = strtok_r (NULL, ": ", &tokp);
			if (temp) {
				if (g_ascii_isdigit (*temp)) {
					tm.tm_min = atoi (temp);
					flags = strtok_r (NULL, ": ", &tokp);
					if (flags && g_ascii_isdigit (*flags)) {
						tm.tm_sec = atoi (flags);
						flags = strtok_r (NULL, ": ", &tokp);
					}
				} else
					flags = temp;
			}
		}

		if (flags != NULL && tm.tm_hour < 12) {
			char buf[256] = "";
			char *str_utf8;
			struct tm pmtm = {0};

			/* Get locale specific "PM", note that it
			 * may not exist */
			pmtm.tm_hour = 17; /* around tea time is always PM */
			if (strftime (buf, sizeof (buf), "%p", &pmtm) == 0)
				strcpy (buf, "");
			buf[sizeof(buf)-1] = '\0';

			str_utf8 = g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);

			/* eek, this may be evil, we are sort of fuzzy here */
			if ((str_utf8 != NULL && strcmp (flags, str_utf8) == 0) ||
			    g_ascii_strcasecmp (flags, buf) == 0)
				tm.tm_hour += 12;

			g_free (str_utf8);
		}

		g_free (string);
	}

	/* FIXME: Eeeeeeeeek! */
	tm.tm_isdst = -1;

	return mktime (&tm);
}

#ifndef GNOME_DISABLE_DEPRECATED_SOURCE

/**
 * gnome_date_edit_get_date:
 * @gde: The GnomeDateEdit widget
 *
 * Deprecated, use #gnome_date_edit_get_time
 *
 * Returns:
 */
time_t
gnome_date_edit_get_date (GnomeDateEdit *gde)
{
	g_warning(_("gnome_date_edit_get_date deprecated, use gnome_date_edit_get_time"));
	return gnome_date_edit_get_time(gde);
}

#endif /* not GNOME_DISABLE_DEPRECATED_SOURCE */


/**
 * gnome_date_edit_set_flags:
 * @gde: The date editor widget whose flags should be changed.
 * @flags: The new bitmask of GnomeDateEditFlags values.
 *
 * Changes the display flags on an existing date editor widget.
 **/
void
gnome_date_edit_set_flags (GnomeDateEdit *gde, GnomeDateEditFlags flags)
{
return;
        GnomeDateEditFlags old_flags;

	g_return_if_fail (gde != NULL);
	g_return_if_fail (GNOME_IS_DATE_EDIT (gde));

        old_flags = gde->_priv->flags;
        gde->_priv->flags = flags;

	if ((flags & GNOME_DATE_EDIT_SHOW_TIME) != (old_flags & GNOME_DATE_EDIT_SHOW_TIME)) {
		if (flags & GNOME_DATE_EDIT_SHOW_TIME) {
			gtk_widget_show (gde->_priv->cal_label);
			gtk_widget_show (gde->_priv->time_entry);
			gtk_widget_show (gde->_priv->time_popup);
		} else {
			gtk_widget_hide (gde->_priv->cal_label);
			gtk_widget_hide (gde->_priv->time_entry);
			gtk_widget_hide (gde->_priv->time_popup);
		}
	}

	if ((flags & GNOME_DATE_EDIT_24_HR) != (old_flags & GNOME_DATE_EDIT_24_HR))
		fill_time_popup (GTK_WIDGET (gde), gde); /* This will destroy the old menu properly */

}

/**
 * gnome_date_edit_get_flags:
 * @gde: The date editor whose flags should be queried.
 *
 * Queries the display flags on a date editor widget.
 *
 * Return value: The current display flags for the specified date editor widget.
 **/
int
gnome_date_edit_get_flags (GnomeDateEdit *gde)
{
	g_return_val_if_fail (gde != NULL, 0);
	g_return_val_if_fail (GNOME_IS_DATE_EDIT (gde), 0);

	return gde->_priv->flags;
}

/**
 * gnome_date_edit_get_initial_time:
 * @gde: The date editor whose initial time should be queried
 *
 * Description:  Queries the initial time that was set using the
 * #gnome_date_edit_set_time or during creation
 *
 * Returns:  The initial time in seconds (standard time_t format)
 **/
time_t
gnome_date_edit_get_initial_time (GnomeDateEdit *gde)
{
	g_return_val_if_fail (gde != NULL, 0);
	g_return_val_if_fail (GNOME_IS_DATE_EDIT (gde), 0);

	return gde->_priv->initial_time;
}
