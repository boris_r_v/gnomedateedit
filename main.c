#include <stdlib.h>
//#include <gtk/gtk.h>
#include <time.h>
#include "gnome-dateedit.h"
static void new_text_into( GtkWidget* w, gpointer g )
{
    g_print( "%s\n", gtk_entry_get_text( GTK_ENTRY(w) ) );
}

static void new_date_into( GtkWidget* w, gpointer g )
{
    time_t _in_time = gnome_date_edit_get_time( GNOME_DATE_EDIT(w) );
    struct tm _tm_zone;
    gchar buff[200];
    localtime_r(&_in_time, &_tm_zone);
    g_snprintf(buff, sizeof(buff), "%.2d:%.2d:%.2d %.2d/%.2d/%.2d(%.4d) utc:%lu", 
	    _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec, 
	    _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900, _in_time  );
    gtk_entry_set_text( GTK_ENTRY(g), buff );
}

static void new_time_into( GtkWidget* w, gpointer g )
{
    time_t _in_time = gnome_date_edit_get_time( GNOME_DATE_EDIT(w) );
    struct tm _tm_zone;
    gchar buff[200];
    localtime_r(&_in_time, &_tm_zone);
    g_snprintf(buff, sizeof(buff), "%.2d:%.2d:%.2d %.2d/%.2d/%.2d(%.4d) utc:%lu", 
	    _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec, 
	    _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900, _in_time  );
    gtk_entry_set_text( GTK_ENTRY(g), buff );
}

int
main(int argc,char *argv[]) 
{
    GtkWidget *window;
    

    gtk_init(&argc,&argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size ( GTK_WINDOW(window), 30, 20 );

    GtkWidget *area = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4 );
    GtkWidget* gde  = gnome_date_edit_new(0, FALSE, FALSE );
    gnome_date_edit_set_popup_range( GNOME_DATE_EDIT( gde ), 0, 23 );
    GtkWidget* entry = gtk_entry_new();
    g_signal_connect(G_OBJECT( entry), "activate", G_CALLBACK(new_text_into), NULL );
    g_signal_connect(G_OBJECT( gde), "time_changed", G_CALLBACK(new_time_into), entry );
    g_signal_connect(G_OBJECT( gde), "date_changed", G_CALLBACK(new_date_into), entry );
    gtk_box_pack_start( GTK_BOX(area), gde, 0, 0, 0 );
    gtk_box_pack_start( GTK_BOX(area), entry, 0, 0, 0 );
    
    gtk_container_add( GTK_CONTAINER(window), area );
    gtk_widget_show_all( window );

    gtk_main();


    return 0;
}
