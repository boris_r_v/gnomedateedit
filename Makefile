PRG= gnome-dateedit-test libgnome-dateedit.so gnome-dateeditmm-test libgnome-dateeditmm.so

CFLAGS  = -fpic -Wall -g -I.
LDFLAGS = -lm

MMCFLAGS  = $(CFLAGS) `pkg-config gtkmm-3.0 --cflags`
MMLDFLAGS = $(LDFLAGS) `pkg-config gtkmm-3.0 --libs`

CFLAGS  += `pkg-config gtk+-3.0 --cflags`
LDFLAGS += `pkg-config gtk+-3.0 --libs`
#CFLAGS+="-DGTK_DISABLE_SINGLE_INCLUDES"
#CFLAGS+="-DGDK_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED"
#CFLAGS+="-DGSEAL_ENABLE"



all: $(PRG)

#C
gnome-dateedit.o:  gnome-dateedit.c
	cc $(CFLAGS) gnome-dateedit.c -c
	@msgfmt ./po/ru.po -o ./po/libgnome-dateedit.mo

main.o:  main.c
	cc $(CFLAGS) main.c -c

gnome-dateedit-test:  gnome-dateedit.o main.o 
	cc  gnome-dateedit.o main.o -o gnome-dateedit-test $(LDFLAGS)

libgnome-dateedit.so: gnome-dateedit.o 
	cc -shared gnome-dateedit.o -o libgnome-dateedit.so $(LDFLAGS)

#C++
gnome-dateeditmm.o:  gnome-dateeditmm.cc
	g++ $(MMCFLAGS) gnome-dateeditmm.cc -c

mainmm.o:  mainmm.cc
	g++ $(MMCFLAGS) mainmm.cc -c

gnome-dateeditmm-test:  gnome-dateeditmm.o mainmm.o gnome-dateedit.o
	g++ gnome-dateedit.o  gnome-dateeditmm.o mainmm.o -o gnome-dateeditmm-test $(MMLDFLAGS)

libgnome-dateeditmm.so: gnome-dateedit.o gnome-dateeditmm.o
	cc -shared gnome-dateedit.o gnome-dateeditmm.o -o libgnome-dateeditmm.so $(MMLDFLAGS)

main_cal: main_cal.c
	gcc  $(CFLAGS) main_cal.c -o main_cal $(LDFLAGS)


install: all

	@sudo cp -v ./po/libgnome-dateedit.mo /usr/share/locale/ru/LC_MESSAGES
	@sudo cp -v *.so /usr/local/lib	
	@[ -d /usr/local/include/gnomedateedit ] || sudo mkdir /usr/local/include/gnomedateedit
	@sudo cp -v *.h /usr/local/include/gnomedateedit
	@sudo cp -vf *.pc /usr/lib/pkgconfig
	@sudo cp -vf ld.so.gde.conf /etc/ld.so.conf.d/gnomedateedit.conf
	@echo "-- Runnig ldconfig -- Libs chache updated --"
	@sudo ldconfig





clean:
	@rm -fv $(PRG)
	@rm -fv *.o
	@rm ./po/*.mo

