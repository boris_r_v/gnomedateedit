#include <time.h>
#include "gnome-dateeditmm.h"
#include <gtkmm.h>

class myGDE: public Gnome::UI::DateEdit
{
	Gtk::Entry& ent;
    public:
	myGDE( Gtk::Entry& );
	~myGDE();
	void on_time_changed();
	void on_date_changed();
	void on_changed();
};

myGDE::myGDE( Gtk::Entry& _ent ): ent(_ent)
{
/*
    There is two way to proccess Gnome::UI::DateEdit signals
    1. Inheritance from Gnome::UI::DateEdit and redefine virutal method
	void on_time_changed();
	void on_date_changed();
    2. Connect Gnome::UI::DateEdit signals to member function( or other )
	I connect signal to method: void on_changed();
*/
    signal_date_changed().connect ( sigc::mem_fun ( *this, &myGDE::on_changed ) );     
    signal_time_changed().connect ( sigc::mem_fun ( *this, &myGDE::on_changed ) );     
}

myGDE::~myGDE()
{
}

void myGDE::on_changed()
{
    time_t _in_time = get_time();
    struct tm _tm_zone;
    gchar buff[200];
    localtime_r(&_in_time, &_tm_zone);
    g_snprintf(buff, sizeof(buff), "%.2d:%.2d:%.2d %.2d/%.2d/%.2d(%.4d) utc: %lu", 
	    _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec, 
	    _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900, _in_time  );
    //g_print("on_changed %s\n", buff );
    ent.set_text( buff );
}

void myGDE::on_time_changed()
{
    time_t _in_time = get_time();
    struct tm _tm_zone;
    gchar buff[200];
    localtime_r(&_in_time, &_tm_zone);
    g_snprintf(buff, sizeof(buff), "%.2d:%.2d:%.2d %.2d/%.2d/%.2d(%.4d) utc: %lu", 
	    _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec, 
	    _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900, _in_time  );
    //g_print("%s\n", buff );
    ent.set_text( buff );
}
void myGDE::on_date_changed()
{
    time_t _in_time = get_time();
    struct tm _tm_zone;
    gchar buff[200];
    localtime_r(&_in_time, &_tm_zone);
    g_snprintf(buff, sizeof(buff), "%.2d:%.2d:%.2d %.2d/%.2d/%.2d(%.4d) utc: %lu", 
	    _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec, 
	    _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900, _in_time  );
    //g_print("%s\n", buff );
    ent.set_text( buff );
}

void changed()
{
}

int
main(int argc,char *argv[]) 
{
    Gtk::Main kit(argc, argv );
    Gtk::Window window( Gtk::WINDOW_TOPLEVEL );
    Gtk::Box box( Gtk::ORIENTATION_VERTICAL, 2 );
    Gtk::Entry entr;
    myGDE mdte( entr );

    box.pack_start( mdte, false, false, 1 );
    box.pack_start( entr, false, false, 1 );
    
    window.add( box );
    window.show_all();

    kit.run();
    return 0;
}
